import java.util.ArrayList;

public class ZorgApp {
   static ArrayList<User> userList = new ArrayList<>();

   public static void main(String[] args) {
      // Create multiple users
      User user1 = new User(1, "John Maru");
      User user2 = new User(2, "Jane ElCamera");
      User user3 = new User(3, "Jack Doe");
      User user4 = new User(4, "Jill Doe");
      User user5 = new User(5, "Jenny Jones");
      // Add each user to the userList
      userList.add(user1);
      userList.add(user2);
      userList.add(user3);
      userList.add(user4);
      userList.add(user5);

      Administration administration = new Administration(userList);
      administration.choosePatientMenu();
   }

   public static ArrayList<User> getUserList() {
      return userList;
   }
}

