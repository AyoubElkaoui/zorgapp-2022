import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

///////////////////////////////////////////////////////////////////////////
// class Administration represents the core of the application by showing
// the main menu, from where all other functionality is accessible, either
// directly or via sub-menus.
//
// An Adminiatration instance needs a User as input, which is passed via the
// constructor to the data member 'çurrentUser'.
// The patient data is available via the data member çurrentPatient.
/////////////////////////////////////////////////////////////////
public class Administration
{
   private static final int STOP = 0;
   private static final int VIEW = 1;
   private static final int EDIT = 2;
   private static final int SWITCH_PATIENT = 3;
   private static final int SWITCH_USER = 4;

   private int choice;
   private ArrayList<Patient> patientList = new ArrayList<>();


   private Patient currentPatient;             // The currently selected patient
   private User    currentUser;               // the current user of the program.

   /////////////////////////////////////////////////////////////////
   // Constructor
   /////////////////////////////////////////////////////////////////
   public Administration(ArrayList<User> userList) {
      // Set the current user to the first user in the list
      currentUser = userList.get(0);
      // Create default patients
      Patient patient1 = new Patient(1, "Van Puffelen", "Pierre", LocalDate.of(2000, 2, 29), 80, 1.80);
      Patient patient2 = new Patient(2, "Van Red", "Monique", LocalDate.of(1997, 1, 25), 56, 1.60);
      Patient patient3 = new Patient(3, "Van True", "Daisy", LocalDate.of(1994, 5, 15), 67, 1.70);
      Patient patient4 = new Patient(4, "Van Das", "Jase", LocalDate.of(1999, 9, 21), 86, 1.88);
      Patient patient5 = new Patient(5, "Van Netr", "Mora", LocalDate.of(1992, 5, 28), 90, 2.05);
      // Add the patients to the patientList
      patientList.add(patient1);
      patientList.add(patient2);
      patientList.add(patient3);
      patientList.add(patient4);
      patientList.add(patient5);
   }

   public void chooseUserMenu() {
      Scanner scanner = new Scanner(System.in);
      do {
         // Print the menu
         System.out.println("Choose a user");
         // Get the user list
         ArrayList<User> userList = ZorgApp.getUserList();
         // Print the user list
         for (User user : userList) {
            System.out.format("[%d] %s %n", user.getUserID(), user.getUserName());
         }
         System.out.print("Enter your choice: ");
         // Get the user choice
         choice = scanner.nextInt();
         // If the user choice is valid, set the current user
         if (choice > 0 && choice <= userList.size()) {
            currentUser = userList.get(choice - 1);
            menu();
         } else if (choice == STOP) {
            // Exit the program
            System.out.println("Goodbye!");
            System.exit(0);
         } else {
            // Invalid choice
            System.out.println("Invalid choice");
         }
      } while (choice != STOP);
   }

   public void choosePatientMenu() {
      Scanner scanner = new Scanner(System.in);
      do {
         System.out.println("Choose a patient");
         System.out.println("[0] Exit");
         for (Patient patient : patientList) {
            // For each patient in the patientList, print the patient number (Index) and the full name
            System.out.println("[" + (patientList.indexOf(patient) + 1) + "] " + patient.fullName());
         }
         System.out.print("Enter your choice: ");
         choice = scanner.nextInt();
         if (choice > 0 && choice <= patientList.size()) {
            // Set the chosen patient as the current patient
            currentPatient = patientList.get(choice - 1);
            menu();
         } else if (choice == STOP) {
            // Exit the program
            System.out.println("Goodbye!");
            System.exit(0);
         } else {
            // Invalid choice
            System.out.println("Invalid choice");
         }
      } while (choice != STOP);
   }


   /////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////
   void menu() {
      System.out.format("\033[33mCurrent user: [%d] %s\033[0m %n", currentUser.getUserID(), currentUser.getUserName());
      var scanner = new Scanner(System.in);  // User input via this scanner.
      do {
         System.out.format("%s\n", "=".repeat(80));
         System.out.format("\033[34mCurrent patient: %s\033[0m %n", currentPatient.fullName());
         ////////////////////////
         // Print menu on screen
         ////////////////////////
         System.out.format("\u001B[31m" + "%d: Exit\n" + "\u001B[0m", STOP);
         System.out.format("%d: View patient data\n", VIEW);
         System.out.format("%d: Edit patient data\n", EDIT);
         System.out.format("%d: Switch patient \n", SWITCH_PATIENT);
         System.out.format("\033[33m" + "%d: Switch user \n" + "\033[0m", SWITCH_USER);

         System.out.print("Enter your choice: ");
         int choice = scanner.nextInt();
         switch (choice) {
            case STOP:
               // Exit the program
               System.out.println("\u001B[31m" + "Goodbye!" + "\u001B[0m");
               System.exit(0);
               break;

            case VIEW:
               // View the patient data
               currentUser.viewPatientData(currentPatient);
               break;

            case EDIT:
               // Edit the patient data
               currentUser.editPatientData(currentPatient);
               break;

            case SWITCH_PATIENT:
               // Switch patient
               choosePatientMenu();
               break;

            case SWITCH_USER:
               // Switch user
               chooseUserMenu();
               break;

            default:
               System.out.println("Please enter a *valid* digit");
               break;
         }
      } while (choice != STOP);
   }
}
