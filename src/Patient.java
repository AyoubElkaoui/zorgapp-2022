import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;

public class Patient
{
   private static final int RETURN      = 0;
   private static final int SURNAME     = 1;
   private static final int FIRSTNAME   = 2;
   private static final int DATEOFBIRTH = 3;
   private static final int LENGTH      = 4;
   private static final int WEIGHT      = 5;

   private int       id;
   private String    surname;
   private String    firstName;
   private LocalDate dateOfBirth;
   private double    length;
   private double    weight;



   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   public String getSurname()
   {
      return surname;
   }
   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   // Method to calculate the age of a patient
   public int calcAge(LocalDate dateOfBirth){
      //Code gets current date
      LocalDate curDate = LocalDate.now();
      /*If else statement that checks if both dates are not null*/
      if ((dateOfBirth != null) && (curDate != null))
      {
         /*if dates are both not null the code will take the birthdate and currentdate and
         calculate the difference the code will calculate the age in years */
         return Period.between(dateOfBirth, curDate).getYears();
      }
      else
      {
         //if one or both dates are null the code will return 0
         return 0;
      }
   }
   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   //this code formats the double in 2 decimals so it looks cleaner
   public static final DecimalFormat df = new DecimalFormat("0.0");
   public static final DecimalFormat df2 = new DecimalFormat("0.00");

   //this code calculates the BMI of the patient
   public String calcBMI(double weight, double length) {
      return df.format(weight / (length * length));
   }

   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   public String getFirstName()
   {
      return firstName;
   }

   ////////////////////////////////////////////////////////////////////////////////
   // Constructor
   ////////////////////////////////////////////////////////////////////////////////
   Patient( int id, String surname, String firstName, LocalDate dateOfBirth, double weight, double length )
   {
      this.id          = id;
      this.surname     = surname;
      this.firstName   = firstName;
      this.dateOfBirth = dateOfBirth;
      this.weight      = weight;
      this.length      = length;
   }

   ////////////////////////////////////////////////////////////////////////////////
   // Display patient data.
   ////////////////////////////////////////////////////////////////////////////////
   public void viewData()
   {
      System.out.format( "===== Patient id=%d ==============================\n", id );
      System.out.format( "%-17s %s\n", "Surname:", surname );
      System.out.format( "%-17s %s\n", "firstName:", firstName );
      System.out.format( "%-17s %s\n", "Date of birth:", dateOfBirth );
      System.out.format( "%-17s %s\n", "Age:", calcAge(dateOfBirth) );
      System.out.format( "%-17s %s\n", "Weight in KG:", weight + " kg");
      System.out.format( "%-17s %s\n", "Length in M:", df2.format(length) + " m");
      System.out.format( "%-17s %s\n", "The Patients BMI:", calcBMI(weight, length) );
   }

   ////////////////////////////////////////////////////////////////////////////////
   // Shorthand for a Patient's full name1
   public void editData()
   {
      System.out.format("What do you want to edit?\n");
      System.out.println("1: Surname");
      System.out.println("2: Firstname");
      System.out.println("3: Date of birth");
      System.out.println("4: Length");
      System.out.println("5: Weight");
      System.out.print("Choose number: ");

      Scanner scanner = new Scanner(System.in);
      int choice = scanner.nextInt();
      switch(choice)
      {
         case SURNAME:
            System.out.print("Type in the Surname: ");
            Scanner surName = new Scanner(System.in);
            surname = surName.nextLine();
         break;
         case FIRSTNAME:
            System.out.print("Type in the Firstname: ");
            Scanner firstname = new Scanner(System.in);
            firstName = firstname.nextLine();
         break;
         case DATEOFBIRTH:
            System.out.print("Type in the birthdate (dd-mm-yyyy): ");
            Scanner dateofbirth = new Scanner(System.in);
            String str = dateofbirth.nextLine();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            dateOfBirth = LocalDate.parse(str, dtf);
         break;
         case LENGTH:
            System.out.print("Type in the length (in meters): ");
            Scanner Length = new Scanner(System.in);
            length = Double.parseDouble(Length.nextLine());
            break;
         case WEIGHT:
            System.out.print("Type in the weight (in kg): ");
            Scanner Weight = new Scanner(System.in);
            weight = Double.parseDouble(Weight.nextLine());
         break;


      }
   }
   ////////////////////////////////////////////////////////////////////////////////
   public String fullName()
   {
      return String.format( "%s %s [%s]", firstName, surname, dateOfBirth.toString(), calcAge(dateOfBirth), weight, length, calcBMI(weight, length));
   }
}
